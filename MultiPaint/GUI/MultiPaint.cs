﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using MultiPaint.DataBags;
using MultiPaint.Helpers;
using MultiPaint.Helpers.Exceptions;

namespace MultiPaint
{
    public partial class MultiPaint : Form
    {
        Connection _connection;
        List<string> _users;

        public MultiPaint(List<string> users, Connection connection)
        {
            InitializeComponent();
            _users = users;
            _connection = connection;
            listUsers.DataSource = _users;
            this.backgroundWorker1.RunWorkerAsync();
            //Thread listeningThread;
            //listeningThread = new Thread(new ThreadStart(Listener));
            //listeningThread.Start();
        }

        public void Listener()
        {
            string response;
            while (true)
            {
                response = _connection.ReadMessage();
                Console.WriteLine("line read");
                if (!string.IsNullOrEmpty(response))
                {
                    Package package = new Package(response);
                    switch (package.Command)
                    {
                        case "JOIN":
                            _users.Add(package.Arguments[0]);
                            listUsers.DataSource = null;
                            listUsers.Items.Clear();
                            listUsers.DataSource = _users;
                            break;
                    }
                }
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            Listener();
        }
    }
}