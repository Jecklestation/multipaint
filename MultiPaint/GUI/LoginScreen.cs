﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using MultiPaint.Helpers;
using MultiPaint.Helpers.Exceptions;

namespace MultiPaint.GUI
{
    public partial class LoginScreen : Form
    {
        string client = "Multipaint";
        string client_version = "1.0";
        List<string> _users;
        Connection _connection;

        public LoginScreen()
        {
            InitializeComponent();
        }

        private void Connect(object sender, EventArgs e)
        {
            if (ValidatedLogin())
            {
                _connection = new Connection(txtServer.Text, Int32.Parse(txtPort.Text));
                MD5 md5Hash = MD5.Create();
                byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(txtPassword.Text));
                StringBuilder passBuilder = new StringBuilder();

                for (int i = 0; i < data.Length; i++)
                {
                    passBuilder.Append(data[i].ToString("x2"));
                }
                string cryptPass = passBuilder.ToString();

                try
                {
                    _users = _connection.Handshake(txtUsername.Text, txtEmail.Text, client, client_version, Environment.OSVersion.Platform.ToString(), cryptPass);
                    this.DialogResult = DialogResult.OK;
                    this.Dispose();
                }
                catch (ErrorException ex)
                {
                    MessageBox.Show(ex.ErrorMessage);
                }
            }
        }

        private bool ValidatedLogin()
        {
            string errors = "";
            if (txtServer.Text == " ")
            {
                errors += "Gelieve een serveradres in te vullen !";
            }
            if (errors == "")
            {
                return true;
            }
            else
            {
                MessageBox.Show(errors);
                return false;
            }
        }

        public List<string> Users
        {
            get { return _users; }
        }

        public Connection MadeConnection
        {
            get { return _connection; }
        }
    }
}