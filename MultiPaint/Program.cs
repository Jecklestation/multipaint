﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using MultiPaint.GUI;

namespace MultiPaint
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            LoginScreen login = new LoginScreen();
            if (login.ShowDialog() == DialogResult.OK)
            {
                Application.Run(new MultiPaint(login.Users, login.MadeConnection));
            }
        }
    }
}