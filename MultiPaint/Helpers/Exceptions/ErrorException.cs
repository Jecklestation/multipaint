﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MultiPaint.DataBags;

namespace MultiPaint.Helpers.Exceptions
{
    internal class ErrorException : Exception
    {
        string _message;

        public ErrorException(Package package)
        {
            _message = "ERROR" + package.Arguments[0];
        }

        public string ErrorMessage
        {
            get { return _message; }
            set { _message = value; }
        }
    }
}