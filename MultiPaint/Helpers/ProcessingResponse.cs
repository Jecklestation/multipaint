﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MultiPaint.DataBags;

namespace MultiPaint.Helpers
{
    internal class ProcessingResponse
    {
        public string[] HELLO(Package package)
        {
            List<string> decodeUsers = new List<string>();
            foreach (string user in package.Arguments)
            {
                decodeUsers.Add(System.Convert.FromBase64String(user).ToString());
            }
            return decodeUsers.ToArray();
        }

        internal Boolean OK(Package package)
        {
            if (package.Command == "OK")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        internal bool WELCOME(Package package)
        {
            if (package.Command == "WELCOME")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}