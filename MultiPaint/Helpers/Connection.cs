﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using MultiPaint.DataBags;
using MultiPaint.Helpers.Exceptions;

namespace MultiPaint.Helpers
{
    public class Connection
    {
        private string _server;
        private Int32 _port;
        NetworkStream _stream;
        private TcpClient client;
        string protocolVersion = "1";
        ProcessingResponse processor;
        StreamReader reader;
        StreamWriter writer;

        public Connection(String server, Int32 port)
        {
            this._server = server;
            this._port = port;
            processor = new ProcessingResponse();
            Connect();
        }

        private void Connect()
        {
            try
            {
                client = new TcpClient(_server, _port);
                NetworkStream stream = client.GetStream();
                _stream = stream;
                reader = new StreamReader(_stream);
                writer = new StreamWriter(_stream);
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine("ArgumentNullException: {0}", e);
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
            }
        }

        public List<string> Handshake(string nickname, string mail, string name, string versie, string os, string pass)
        {
            Package package;
            List<string> users = new List<string>();
            SendMessage("MULTIPAINT " + protocolVersion);
            package = new Package(ReadMessage());
            if (processor.OK(package) == false)
            {
                throw new ErrorException(package);
            }
            SendMessage("HELLO" + " " + nickname + " " + mail + " " + name + " " + versie + " " + os + " " + pass);
            package = new Package(ReadMessage());
            if (processor.OK(package) == false)
            {
                throw new ErrorException(package);
            }
            package = new Package(ReadMessage());
            package = new Package(ReadMessage());
            package = new Package(ReadMessage());
            if (processor.WELCOME(package) == false)
            {
                foreach (string user in package.Arguments)
                {
                    users.Add(user);
                }

                package = new Package(ReadMessage());
            }

            return users;
        }

        public string ReadMessage()
        {
            string responseData = reader.ReadLine();
            return responseData;
        }

        public void SendMessage(String message)
        {
            writer.WriteLine(message);
            writer.Flush();
        }
    }
}