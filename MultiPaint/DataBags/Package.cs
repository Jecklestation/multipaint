﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MultiPaint.DataBags
{
    internal class Package
    {
        private string prefix;
        private string command;
        private List<string> arguments = new List<string>();
        private string message;

        public Package()
        {
        }

        public Package(string response)//:prefic COMMAND arg1 arg2 :message
        {
            string[] brokenPackage = response.Split(new Char[] { ' ' });

            if (brokenPackage[0].Contains(':'))
            {
                prefix = brokenPackage[0].Remove(0, 1);
                command = brokenPackage[1];
                Boolean messageParts = false;
                for (int x = 2; x <= brokenPackage.Length - 1; x++)
                {
                    if (messageParts)
                    {
                        message = message + " " + brokenPackage[x];
                        continue;
                    }

                    if (brokenPackage[x].Contains(':') && !messageParts)
                    {
                        messageParts = true;
                        message = message + " " + brokenPackage[x].Remove(0, 1);
                    }
                    else { arguments.Add(brokenPackage[x]); }
                }
            }
            else
            {
                command = brokenPackage[0];

                Boolean messageParts = false;
                for (int x = 1; x <= brokenPackage.Length - 1; x++)
                {
                    if (messageParts)
                    {
                        message = message + " " + brokenPackage[x];
                        continue;
                    }

                    if (brokenPackage[x].Contains(':') && !messageParts)
                    {
                        messageParts = true;
                        message = message + " " + brokenPackage[x].Remove(0, 1);
                    }
                    else { arguments.Add(brokenPackage[x]); }
                }
               
            }
        }

        public string Prefix
        {
            get { return prefix; }
            set { prefix = value; }
        }

        public string Command
        {
            get { return command; }
            set { command = value; }
        }

        public List<string> Arguments
        {
            get { return arguments; }
            set
            {
                foreach (string arg in value)
                {
                    arguments.Add(arg);
                };
            }
        }

        public string Message
        {
            get { return message; }
            set { message = value; }
        }

        public string ToProtocol()
        {
            string protocol = Command + " ";

            if (Arguments != null)
            {
                foreach (string arg in Arguments)
                {
                    protocol = protocol + " " + arg;
                }
            }
            if (message != null)
            {
                protocol = protocol + " :" + Message;
            }
            return protocol;
        }
    }
}